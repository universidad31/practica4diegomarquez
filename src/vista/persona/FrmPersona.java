/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.persona;

import controlador.tda.lista.ListaEnlazadaServices;
import controlador.persona.PersonaController;
import controlador.persona.servicios.PersonaServicio;
import controlador.tda.lista.ListaEnlazada;
import controlador.utiles.TipoOrdenacion;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import vista.persona.tablas.TablaPersonas;

/**    
 *
 * @author diego
 */
public class FrmPersona extends javax.swing.JDialog {

    private PersonaServicio autorController = new PersonaServicio();
    private TablaPersonas ta = new TablaPersonas();
    private int pos = -1;

    /**
     * Creates new form FrmAutor
     */
    public FrmPersona(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        buttonGroup1.add(radioAscendente);
        buttonGroup1.add(radioDescendente);
        buttonGroup1.add(radioNinguno);
        limpiar();
    }

    /**
     * Creates new form FrmPersona
     */
    private void cargarTabla() {

        ta.setLista(autorController.getListaArchivo());
        tbl_tabla.setModel(ta);
        tbl_tabla.updateUI();
    }

    private void limpiar() {
        txt_apellidos.setText("");
        txt_nombres.setText("");
        txt_identificacion.setText("");
        autorController.setAutor(null);
        cargarTabla();
        radioNinguno.setSelected(true);
    }

    private void guardar() {
        if (txt_apellidos.getText().trim().isEmpty() || txt_nombres.getText().trim().isEmpty() || txt_identificacion.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Campos vacios", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            autorController.getAutor().setApellidos(txt_apellidos.getText().trim());
            autorController.getAutor().setNombres(txt_nombres.getText().trim());
            autorController.getAutor().setIdentificacion(txt_identificacion.getText().trim());

            if (autorController.getAutor().getId() == null) {
                //if (autorController.getListaAutores().insertarAlFinal(autorController.getAutor())) {
                if (autorController.guardar()) {
                    JOptionPane.showMessageDialog(null, "Se ha guardadao correctamente", "OK", JOptionPane.INFORMATION_MESSAGE);
                    limpiar();
                } else {
                    JOptionPane.showMessageDialog(null, "No se guardo", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                //if (autorController.getLista().modificarDatoPosicion(pos, autorController.getAutor())) {
                if (autorController.modificar(pos)) {

                    JOptionPane.showMessageDialog(null, "Se ha modificado correctamente", "OK", JOptionPane.INFORMATION_MESSAGE);
                    limpiar();
                } else {
                    JOptionPane.showMessageDialog(null, "No se pudo modificar", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

    private void ordenar() throws Exception {
        if (cbx_metodoOrdenacion.getSelectedIndex() == 0) {
            txt_metodoSeleccionado.setText("Ha seleccionado el método SHELL");
            if (radioNinguno.isSelected()) {
                cargarTabla();
            } else {
                String atributo = "";

                if (jComboBox1.getSelectedIndex() == 0) {
                    atributo = "nombres";
                }

                if (jComboBox1.getSelectedIndex() == 1) {
                    atributo = "apellidos";
                }
                if (jComboBox1.getSelectedIndex() == 2) {
                    atributo = "identificacion";
                }

                ListaEnlazadaServices lista = new ListaEnlazadaServices();
                if (radioAscendente.isSelected()) {
                    lista.setLista(autorController.getListaArchivo().getLista().Shell_ord(atributo, TipoOrdenacion.ASCENDENTE));

                } else if (radioDescendente.isSelected()) {
                    lista.setLista(autorController.getListaArchivo().getLista().Shell_ord(atributo, TipoOrdenacion.DESCENDENTE));
                }
                ta.setLista(lista);
                tbl_tabla.setModel(ta);
                tbl_tabla.updateUI();
            }
        }else{
            txt_metodoSeleccionado.setText("Ha seleccionado el método QuickSort");
            if (radioNinguno.isSelected()) {
                cargarTabla();
            } else {
                String atributo = "";

                if (jComboBox1.getSelectedIndex() == 0) {
                    atributo = "nombres";
                }

                if (jComboBox1.getSelectedIndex() == 1) {
                    atributo = "apellidos";
                }
                if (jComboBox1.getSelectedIndex() == 2) {
                    atributo = "identificacion";
                }

                ListaEnlazadaServices lista = new ListaEnlazadaServices();
                if (radioAscendente.isSelected()) {
                    lista.setLista(autorController.getListaArchivo().getLista().quickSort_ord(atributo, TipoOrdenacion.ASCENDENTE));

                } else if (radioDescendente.isSelected()) {
                    lista.setLista(autorController.getListaArchivo().getLista().quickSort_ord(atributo, TipoOrdenacion.DESCENDENTE));
                }
                ta.setLista(lista);
                tbl_tabla.setModel(ta);
                tbl_tabla.updateUI();
            }
            
        }

    }
    

     


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_apellidos = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txt_nombres = new javax.swing.JTextField();
        btn_cancelar = new javax.swing.JButton();
        btn_guardar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txt_identificacion = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        radioDescendente = new javax.swing.JRadioButton();
        radioNinguno = new javax.swing.JRadioButton();
        radioAscendente = new javax.swing.JRadioButton();
        jPanel5 = new javax.swing.JPanel();
        jComboBox1 = new javax.swing.JComboBox<>();
        cbx_metodoOrdenacion = new javax.swing.JComboBox<>();
        txt_metodoSeleccionado = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        cbx_busqueda = new javax.swing.JComboBox<>();
        txt_buscar = new javax.swing.JTextField();
        btn_buscar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_tabla = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Autores");
        getContentPane().setLayout(null);

        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setLayout(null);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Registrar Personas"));
        jPanel2.setLayout(null);

        jLabel1.setText("Apellidos:");
        jPanel2.add(jLabel1);
        jLabel1.setBounds(40, 100, 90, 18);
        jPanel2.add(txt_apellidos);
        txt_apellidos.setBounds(30, 130, 250, 24);

        jLabel2.setText("Nombres:");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(40, 30, 90, 18);
        jPanel2.add(txt_nombres);
        txt_nombres.setBounds(30, 60, 250, 24);

        btn_cancelar.setText("CANCELAR");
        jPanel2.add(btn_cancelar);
        btn_cancelar.setBounds(160, 250, 110, 24);

        btn_guardar.setText("GUARDAR");
        btn_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarActionPerformed(evt);
            }
        });
        jPanel2.add(btn_guardar);
        btn_guardar.setBounds(30, 250, 110, 24);

        jLabel3.setText("Identificación:");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(40, 170, 90, 18);
        jPanel2.add(txt_identificacion);
        txt_identificacion.setBounds(30, 200, 250, 24);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(10, 10, 300, 300);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Gestión de Tabla"));
        jPanel3.setLayout(null);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("ORDENAR"));
        jPanel4.setLayout(null);

        radioDescendente.setFont(new java.awt.Font("Liberation Sans", 0, 10)); // NOI18N
        radioDescendente.setText("DESCENDENTE");
        radioDescendente.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radioDescendenteItemStateChanged(evt);
            }
        });
        jPanel4.add(radioDescendente);
        radioDescendente.setBounds(140, 70, 110, 20);

        radioNinguno.setFont(new java.awt.Font("Liberation Sans", 0, 10)); // NOI18N
        radioNinguno.setText("NINGUNO");
        radioNinguno.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radioNingunoItemStateChanged(evt);
            }
        });
        jPanel4.add(radioNinguno);
        radioNinguno.setBounds(270, 70, 70, 19);

        radioAscendente.setFont(new java.awt.Font("Liberation Sans", 0, 10)); // NOI18N
        radioAscendente.setText("ASCENDENTE");
        radioAscendente.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radioAscendenteItemStateChanged(evt);
            }
        });
        jPanel4.add(radioAscendente);
        radioAscendente.setBounds(20, 70, 93, 19);

        jPanel5.setLayout(null);

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nombre", "Apellidos", "Identificación" }));
        jPanel5.add(jComboBox1);
        jComboBox1.setBounds(180, 10, 140, 24);

        cbx_metodoOrdenacion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Shell", "QuickSort" }));
        cbx_metodoOrdenacion.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbx_metodoOrdenacionItemStateChanged(evt);
            }
        });
        jPanel5.add(cbx_metodoOrdenacion);
        cbx_metodoOrdenacion.setBounds(30, 10, 130, 24);

        jPanel4.add(jPanel5);
        jPanel5.setBounds(10, 20, 340, 50);

        txt_metodoSeleccionado.setFont(new java.awt.Font("Liberation Sans", 0, 10)); // NOI18N
        jPanel4.add(txt_metodoSeleccionado);
        txt_metodoSeleccionado.setBounds(20, 100, 320, 20);

        jPanel3.add(jPanel4);
        jPanel4.setBounds(20, 30, 360, 130);

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder("BÚSQUEDA BINARIA"));
        jPanel6.setLayout(null);

        cbx_busqueda.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nombre", "Apellidos", "Identificación" }));
        jPanel6.add(cbx_busqueda);
        cbx_busqueda.setBounds(40, 30, 140, 24);
        jPanel6.add(txt_buscar);
        txt_buscar.setBounds(20, 60, 310, 24);

        btn_buscar.setText("BUSCAR");
        jPanel6.add(btn_buscar);
        btn_buscar.setBounds(190, 30, 120, 24);

        jPanel3.add(jPanel6);
        jPanel6.setBounds(20, 170, 360, 110);

        jPanel1.add(jPanel3);
        jPanel3.setBounds(320, 10, 400, 300);

        tbl_tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbl_tabla);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(10, 330, 710, 170);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(10, 10, 740, 520);

        setSize(new java.awt.Dimension(766, 578));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarActionPerformed
        // TODO add your handling code here:
        guardar();
    }//GEN-LAST:event_btn_guardarActionPerformed

    private void radioAscendenteItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radioAscendenteItemStateChanged
        // TODO add your handling code here:
        try {
            ordenar();
        } catch (Exception e) {
            System.out.println("Error" + e);
        }
    }//GEN-LAST:event_radioAscendenteItemStateChanged

    private void radioDescendenteItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radioDescendenteItemStateChanged
        // TODO add your handling code here:
        try {
            ordenar();
        } catch (Exception e) {
            System.out.println("Error" + e);
        }
    }//GEN-LAST:event_radioDescendenteItemStateChanged

    private void radioNingunoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radioNingunoItemStateChanged
        // TODO add your handling code here:
        try {
            ordenar();
        } catch (Exception e) {
            System.out.println("Error" + e);
        }
    }//GEN-LAST:event_radioNingunoItemStateChanged

    private void cbx_metodoOrdenacionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbx_metodoOrdenacionItemStateChanged
        // TODO add your handling code here:
        try {
            ordenar();
        } catch (Exception e) {
        }
    }//GEN-LAST:event_cbx_metodoOrdenacionItemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmPersona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmPersona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmPersona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmPersona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmPersona dialog = new FrmPersona(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_buscar;
    private javax.swing.JButton btn_cancelar;
    private javax.swing.JButton btn_guardar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> cbx_busqueda;
    private javax.swing.JComboBox<String> cbx_metodoOrdenacion;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JRadioButton radioAscendente;
    private javax.swing.JRadioButton radioDescendente;
    private javax.swing.JRadioButton radioNinguno;
    private javax.swing.JTable tbl_tabla;
    private javax.swing.JTextField txt_apellidos;
    private javax.swing.JTextField txt_buscar;
    private javax.swing.JTextField txt_identificacion;
    private javax.swing.JLabel txt_metodoSeleccionado;
    private javax.swing.JTextField txt_nombres;
    // End of variables declaration//GEN-END:variables
}
