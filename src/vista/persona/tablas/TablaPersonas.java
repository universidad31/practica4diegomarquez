/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.persona.tablas;

import controlador.tda.lista.ListaEnlazadaServices;
import javax.swing.table.AbstractTableModel;
import modelo.persona.Persona;

/**
 *
 * @author sebastian
 */
public class TablaPersonas extends AbstractTableModel {
    private ListaEnlazadaServices<Persona> lista;

    public ListaEnlazadaServices<Persona> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazadaServices<Persona> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public int getRowCount() {
        return lista.getSize();
    }

    @Override
    public String getColumnName(int column) {
        switch(column) {
            case 0: return "Nro";
            case 1: return "Nombres";
            case 2: return "Apellidos";
            case 3: return "Identificación";
            default: return null;
        }
    }

    @Override
    public Object getValueAt(int arg0, int arg1) {
        Persona autor = lista.obtenerDato(arg0);
        switch(arg1) {
            case 0: return (arg0+1);
            case 1: return autor.getNombres();
            case 2: return autor.getApellidos();
            case 3: return autor.getIdentificacion();
            default: return null;
        }
    }
    
    
    
    
}
