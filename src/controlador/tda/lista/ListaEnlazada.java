/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.tda.lista;

import controlador.tda.lista.exception.PosicionException;

import controlador.utiles.Utilidades;

import controlador.utiles.TipoOrdenacion;
import static controlador.utiles.Utilidades.getMethod;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sebastian
 */
//E   T    K   V
//E = T
@XmlRootElement

public class ListaEnlazada<E> {

    private NodoLista<E> cabecera;

    private Integer size;

    @XmlElement
    public NodoLista<E> getCabecera() {
        return cabecera;
    }

    public void setCabecera(NodoLista<E> cabecera) {
        this.cabecera = cabecera;
    }

    /**
     * Constructor de la clase se inicializa la lista en null y el tamanio en 0
     */
    public ListaEnlazada() {
        cabecera = null;
        size = 0;
    }

    /**
     * Permite ver si la lista esta vacia
     *
     * @return Boolean true si esta vacia, false si esta llena
     */
    public Boolean estaVacia() {
        return cabecera == null;
    }

    private void insertar(E dato) {
        NodoLista<E> nuevo = new NodoLista<>(dato, null);
        if (estaVacia()) {
            cabecera = nuevo;
        } else {
            NodoLista<E> aux = cabecera;
            while (aux.getSiguiente() != null) {
                aux = aux.getSiguiente();
            }
            aux.setSiguiente(nuevo);
        }
        size++;
    }

    public void insertarCabecera(E dato) {
        if (estaVacia()) {
            insertar(dato);
        } else {
            NodoLista<E> nuevo = new NodoLista<>(dato, null);

            nuevo.setSiguiente(cabecera);
            cabecera = nuevo;
            size++;
        }
    }

    public void insertar(E dato, Integer pos) throws PosicionException {
        //lista size = 1
        if (estaVacia()) {
            insertar(dato);
        } else if (pos >= 0 && pos < size) {
            NodoLista<E> nuevo = new NodoLista<>(dato, null);
            if (pos == (size - 1)) {
                insertar(dato);

            } else {

                NodoLista<E> aux = cabecera;
                for (int i = 0; i < pos - 1; i++) {
                    aux = aux.getSiguiente();
                }
                NodoLista<E> siguiente = aux.getSiguiente();
                aux.setSiguiente(nuevo);
                nuevo.setSiguiente(siguiente);
                size++;
            }

        } else {
            throw new PosicionException("Error en insertar: No existe la posicion dada");
        }
    }

    public void imprimir() {
        System.out.println("**************************");
        NodoLista<E> aux = cabecera;
        for (int i = 0; i < getSize(); i++) {
            System.out.print(aux.getDato().toString() + "\t");
            aux = aux.getSiguiente();
        }
        System.out.println("\n" + "**************************");
    }

    public Integer getSize() {
        return size;
    }

    /**
     * Metodo que permite obtener un dato segun la posicion
     *
     * @param pos posicion en la lista
     * @return Elemento
     */
    public E obtenerDato(Integer pos) throws PosicionException {
        if (!estaVacia()) {
            if (pos >= 0 && pos < size) {
                E dato = null;
                if (pos == 0) {
                    dato = cabecera.getDato();
                } else {
                    NodoLista<E> aux = cabecera;
                    for (int i = 0; i < pos; i++) {
                        aux = aux.getSiguiente();
                    }
                    dato = aux.getDato();
                }

                return dato;
            } else {
                throw new PosicionException("Error en obtener dato: No existe la posicion dada");
            }

        } else {
            throw new PosicionException("Error en obtener dato: La lista esta vacia, por endde no hay esa posicion");
        }
    }

    public E eliminarDato(Integer pos) throws PosicionException {
        E auxDato = null;
        if (!estaVacia()) {
            if (pos >= 0 && pos < size) {
                if (pos == 0) {
                    auxDato = cabecera.getDato();
                    cabecera = cabecera.getSiguiente();
                    size--;
                } else {
                    NodoLista<E> aux = cabecera;
                    for (int i = 0; i < pos - 1; i++) {
                        aux = aux.getSiguiente();
                    }
                    auxDato = aux.getDato();
                    NodoLista<E> proximo = aux.getSiguiente();
                    aux.setSiguiente(proximo.getSiguiente());
                    size--;
                }
                return auxDato;

            } else {
                throw new PosicionException("Error en eliminar dato: No existe la posicion dada");
            }

        } else {
            throw new PosicionException("Error en eliminar dato: La lista esta vacia, por endde no hay esa posicion");
        }
    }

    public void vaciar() {
        cabecera = null;
        size = 0;
        //en c utilizamos el free
        //malloc
    }

    public void modificarDato(Integer pos, E datoM) throws PosicionException {
        if (!estaVacia()) {
            if (pos >= 0 && pos < size) {
                // E dato = null;
                if (pos == 0) {
                    cabecera.setDato(datoM);
                } else {
                    NodoLista<E> aux = cabecera;
                    for (int i = 0; i < pos; i++) {
                        aux = aux.getSiguiente();
                    }
                    aux.setDato(datoM);
                }

            } else {
                throw new PosicionException("Error en obtener dato: No existe la posicion dada");
            }

        } else {
            throw new PosicionException("Error en obtener dato: La lista esta vacia, por endde no hay esa posicion");
        }
    }

    public E[] toArray() {
        // E[] matriz = (E[]) (new Object[this.size]);
        Class<E> clazz = null;
        E[] matriz = null;
        if (this.size > 0) {
            matriz = (E[]) java.lang.reflect.Array.newInstance(cabecera.getDato().getClass(), this.size);
            NodoLista<E> aux = cabecera;
            for (int i = 0; i < this.size; i++) {
                matriz[i] = aux.getDato();
                //System.out.print(auxiliar.getDato().toString() + "\t");
                aux = aux.getSiguiente();
            }
        }

        return matriz;
    }

    public ListaEnlazada<E> toList(E[] matriz) {
        //E[] matriz = (E[]) (new Object[this.size]);
        this.vaciar();
        for (int i = 0; i < matriz.length; i++) {
            this.insertar(matriz[i]);
        }
        return this;
    }

    public ListaEnlazada<E> Shell_ord(String atributo, TipoOrdenacion tipoOrdenacion) throws Exception {
        Class<E> clazz = null;
        E[] matriz = null;
        if (size > 0) {
            clazz = (Class<E>) cabecera.getDato().getClass();
            Boolean isObject = Utilidades.isObject(clazz);
            matriz = toArray();
            int a, salto;
            boolean intervalo;

            for (salto = matriz.length / 2; salto != 0; salto /= 2) {
                intervalo = true;

                while (intervalo) {
                    intervalo = false;

                    for (a = salto; a < matriz.length; a++) {
                        if (isObject) {
                            Field field = Utilidades.getField(atributo, clazz);
                            Method method = getMethod("get" + Utilidades.capitalizar(atributo), matriz[a - salto].getClass());
                            Method method1 = getMethod("get" + Utilidades.capitalizar(atributo), matriz[a].getClass());
                            Object[] aux = evaluaCambiarDato(field.getType(), matriz[a - salto], matriz[a], method, method1, tipoOrdenacion, a - salto);
                            if (aux[0] != null) {
                                E temp = matriz[a];
                                matriz[a] = matriz[a - salto];
                                matriz[a - salto] = temp;
                                intervalo = true;
                            }
                        } else {
                            Object[] aux = evaluaCambiarDatoNoObjeto(clazz, matriz[a - salto], matriz[a], tipoOrdenacion, a - salto);
                            if (aux[0] != null) {
                                E temp = matriz[a];
                                matriz[a] = matriz[a - salto];
                                matriz[a - salto] = temp;
                                intervalo = true;
                            }

                        }
                    }
                }
            }

        }
        toList(matriz);

        return this;

    }
    
 public ListaEnlazada<E> quickSort_ord(String atributo, TipoOrdenacion tipoOrdenacion) throws PosicionException, Exception {
        Class<E> clazz = null;
        E[] matriz = null;
        if (size > 0) {
            clazz = (Class<E>) cabecera.getDato().getClass();
            Field field = Utilidades.getField(atributo, clazz);
            matriz = toArray();
            desarrolloQuickSort(field.getType(), matriz, 0, matriz.length - 1, atributo, tipoOrdenacion);
        }
        if (matriz != null) {
            toList(matriz);
        }
        return this;
    }

    private void desarrolloQuickSort(Class clazz, E[] matriz, int primero, int ulti, String atributo, TipoOrdenacion tipoOrdenacion) throws Exception {
        E pivote = matriz[primero];
        int i = primero;
        int j = ulti;

        while (i < j) {
            Method method = getMethod("get" + Utilidades.capitalizar(atributo), matriz[i].getClass());
            Method method1 = getMethod("get" + Utilidades.capitalizar(atributo), matriz[j].getClass());
            Method method2 = getMethod("get" + Utilidades.capitalizar(atributo), pivote.getClass());
            if (Utilidades.isNumber(clazz)) {
                if (tipoOrdenacion.toString().equalsIgnoreCase(TipoOrdenacion.ASCENDENTE.toString())) {
                    while (((Number) method.invoke(matriz[i])).doubleValue() <= ((Number) method2.invoke(pivote)).doubleValue() && i < j) {
                        i++;
                    }
                    while (((Number) method1.invoke(matriz[j])).doubleValue() > ((Number) method2.invoke(pivote)).doubleValue()) {
                        j--;
                    }
                } else {
                    while (((Number) method.invoke(matriz[i])).doubleValue() >= ((Number) method2.invoke(pivote)).doubleValue() && i < j) {
                        i++;
                    }
                    while (((Number) method1.invoke(matriz[j])).doubleValue() < ((Number) method2.invoke(pivote)).doubleValue()) {
                        j--;
                    }
                }
                if (i < j) {
                    E aux = matriz[i];
                    matriz[i] = matriz[j];
                    matriz[j] = aux;
                }

            } else if (Utilidades.isString(clazz)) {
                if (tipoOrdenacion.toString().equalsIgnoreCase(TipoOrdenacion.ASCENDENTE.toString())) {
                    while (((String) method.invoke(matriz[i])).toLowerCase().compareTo(((String) method2.invoke(pivote)).toLowerCase()) <= 0 && i < j) {
                        i++;
                    }
                    while (((String) method1.invoke(matriz[j])).toLowerCase().compareTo(((String) method2.invoke(pivote)).toLowerCase()) > 0) {
                        j--;
                    }
                } else {
                    while (((String) method.invoke(matriz[i])).toLowerCase().compareTo(((String) method2.invoke(pivote)).toLowerCase()) >= 0 && i < j) {
                        i++;
                    }
                    while (((String) method1.invoke(matriz[j])).toLowerCase().compareTo(((String) method2.invoke(pivote)).toLowerCase()) < 0) {
                        j--;
                    }
                }
                if (i < j) {
                    E auxiliar = matriz[i];
                    matriz[i] = matriz[j];
                    matriz[j] = auxiliar;
                }

            } else if (Utilidades.isCharacter(clazz)) {
                if (tipoOrdenacion.toString().equalsIgnoreCase(TipoOrdenacion.ASCENDENTE.toString())) {
                    while (((Character) method.invoke(matriz[i])) <= ((Character) method2.invoke(pivote)) && i < j) {
                        i++;
                    }
                    while (((Character) method1.invoke(matriz[j])) > ((Character) method2.invoke(pivote))) {
                        j--;
                    }
                } else {
                    while (((Character) method.invoke(matriz[i])) >= ((Character) method2.invoke(pivote)) && i < j) {
                        i++;
                    }
                    while (((Character) method1.invoke(matriz[j])) < ((Character) method2.invoke(pivote))) {
                        j--;
                    }
                }
                if (i < j) {
                    E auxiliar = matriz[i];
                    matriz[i] = matriz[j];
                    matriz[j] = auxiliar;
                }

            }
        }
        matriz[primero] = matriz[j];
        matriz[j] = pivote;

        if (primero < j - 1) {
            desarrolloQuickSort(clazz, matriz, primero, j - 1, atributo, tipoOrdenacion);
        }

        if (j + 1 < ulti) {
            desarrolloQuickSort(clazz, matriz, j + 1, ulti, atributo, tipoOrdenacion);
        }

    }
    public ListaEnlazada<E> busquedaBinaria(String atributo, Object dato) throws Exception {
        Class<E> clazz = null;
        E[] matriz = null;
        ListaEnlazada<E> resultado = new ListaEnlazada<>();
        E aux = null;
        if (size > 0) {
            matriz = toArray();           
            clazz = (Class<E>) cabecera.getDato().getClass();//primitivo, Dato envolvente, Object
            Boolean isObject = Utilidades.isObject(clazz);//si es objeto
            if (isObject) {
                Field field = Utilidades.getField(atributo, clazz);
                for (int i = 0; i < matriz.length; i++) {
                    Method method1 = getMethod("get" + Utilidades.capitalizar(atributo), matriz[i].getClass());
                    if (field.getType().getSuperclass().getSimpleName().equalsIgnoreCase("Number")
                            && dato.getClass().getSuperclass().getSimpleName().equalsIgnoreCase("Number")) {
                        int primero = 0;
                        int ultimo = matriz.length - 1;
                        int intermedio;
                        Number datoJ = (Number) dato;
                        Number datoJ1 = (Number) method1.invoke(matriz[i]);
                        while (primero <= ultimo) {
                            intermedio = (primero + ultimo) / 2;
                            if (datoJ.doubleValue() == datoJ1.doubleValue()) {
                                aux = (E) matriz[i];
                                if (aux != null) {
                                    resultado.insertar(aux);
                                }
                                break;
                            } else {
                                if (datoJ1.doubleValue() < datoJ.doubleValue()) {
                                    primero = intermedio + 1;
                                } else {
                                    ultimo = intermedio - 1;
                                }
                            }
                        }
                    } else if (Utilidades.isString(field.getType()) && Utilidades.isString(dato.getClass())) {
                        String datoJ = (String) dato;
                        String datoJ1 = (String) method1.invoke(matriz[i]);
                        int primero = 0;
                        int ultimo = matriz.length - 1;
                        int intermedio = 0;
                        while (primero <= ultimo) {
                            intermedio = (primero + ultimo) / 2;
                            if (datoJ1.toLowerCase().startsWith(datoJ.toLowerCase())
                                    || datoJ1.toLowerCase().endsWith(datoJ.toLowerCase())
                                    || datoJ1.toLowerCase().equalsIgnoreCase(datoJ.toLowerCase())) {
                                aux = (E) matriz[i];
                                if (aux != null) {
                                    resultado.insertar(aux);
                                }
                                break;
                            } else {
                                if (datoJ1.toLowerCase().equalsIgnoreCase(datoJ.toLowerCase())) {
                                    primero = intermedio + 1;
                                } else {
                                    ultimo = intermedio - 1;
                                }
                            }

                        }

                    } else if (Utilidades.isCharacter(field.getType()) && Utilidades.isCharacter(dato.getClass())) {
                        Character datoJ = (Character) dato;
                        Character datoJ1 = (Character) method1.invoke(matriz[i]);
                        int primero = 0;
                        int ultimo = matriz.length - 1;
                        int intermedio = 0;
                        while (primero <= ultimo) {
                            intermedio = (primero + ultimo) / 2;
                            if (datoJ.charValue() == datoJ1.charValue()) {
                                aux = (E) matriz[i];
                                if (aux != null) {
                                    resultado.insertar(aux);
                                }
                                break;
                            } else {
                                if (datoJ1.charValue() < datoJ.charValue()) {
                                    primero = intermedio + 1;
                                } else {
                                    ultimo = intermedio - 1;
                                }

                            }

                        }

                    }

                }

            }
        }

        return resultado;
    }


//    public ListaEnlazada<E> ordenar_Shell(String atributo, TipoOrdenacion tipoOrdenacion) throws Exception {
//        Class<E> clazz = null;
//        E[] matriz = null;
//        if (size > 0) {
//            matriz = toArray();
//            E t = null;
//            clazz = (Class<E>) cabecera.getDato().getClass();//primitivo, Dato envolvente, Object
//            Boolean isObject = Utilidades.isObject(clazz);//si es objeto
//
//            Integer i, j, k = 0;
//            Integer n = matriz.length;
//            Integer intervalo = n / 2;
//            //boolean ordenado;
//
//            while (intervalo > 0) {
//                for (i = intervalo; i < n; i++) {
//                    j = i - intervalo;
//                    while (j >= 0) {
//                        k = j + intervalo;
//
//                        if (isObject) {
//                            Field field = Utilidades.getField(atributo, clazz);
//                            Method method = getMethod("get" + Utilidades.capitalizar(atributo), matriz[j].getClass());
//                            Method method1 = getMethod("get" + Utilidades.capitalizar(atributo), matriz[k].getClass());
//                            j = evaluaCambiarDatoNoObjetoShell(field.getType(), matriz[j], matriz[k], metodd, metodo1, orden, j, matriz, intervalo);
//                             
//                        } else {
//                            j = evaluaCambiarDatoNoObjetoShell(clazz, matriz[j],matriz[k], t, method, method1, j, matriz)
//                        }
//
//                    }
//
//                }
//                intervalo = intervalo / 2;
//
//            }
//
//        }
//        if (matriz != null) {
//            toList(matriz);
//        }
//        return this;
//    }
// 

//    private Integer evaluaCambiarDatoNoObjetoShell(Class clazz, E auxJ, E auxK, Method method, Method method1, Integer j, E[] matriz) throws Exception {
//
//        if (clazz.getSuperclass().getSimpleName().equalsIgnoreCase("Number")) {
//            Number datoJ = (Number) method.invoke(auxJ);
//            Number datoJ1 = (Number) method1.invoke(auxK);
//            if (TipoOrdenacion.toString().equalsIgnoreCase(TipoOrdenacion.ASCENDENTE.toString())) {
//                if ((datoJ.doubleValue() <= datoJ1.doubleValue())) {
//                    j = -1;
//                    //  cambioBurbuja(j, matriz);
//                } else {
//                    intercambiar(matriz, j, j + 1);
//                    j = intervalo;
//                }
//                return j;
//            } else {
//                if ((datoJ.doubleValue() >= datoJ1.doubleValue())) {
//                    j=-1;
//                }else{
//                    intercambiar(matriz, j, j+1);
//                    j == intervalo;
//                }
//            }
//
//        } else if (Utilidades.isCharacter(clazz)) {
//            Character datoJ = (Character) auxJ;
//            Character datoJ1 = (Character) auxJ1;
//            if (tipoOrdenacion.toString().equalsIgnoreCase(TipoOrdenacion.ASCENDENTE.toString())) {
//                if (datoJ > datoJ1) {
//                    //cambioBurbuja(j, matriz);
//                    auxiliar[0] = auxJ1;
//                    auxiliar[1] = j;
//                }
//            } else {
//                if (datoJ < datoJ1) {
//                    //cambioBurbuja(j, matriz);
//                    auxiliar[0] = auxJ1;
//                    auxiliar[1] = j;
//                }
//            }
//
//        }
//        return auxiliar;
//    }

      /**
     * Permite hacer el cambio con datos que no son objetos
     *
     * @param clazz El tipo de clase q permite identificar q tipo de dato es
     * @param auxJ Dato en la posicion J
     * @param auxJ1 Dato en la posicion J + 1
     * @param tipoOrdenacion El tipo de ordenacion si es Ascendente o
     * Descendente
     * @param j Posicion
     * @throws Exception
     */

    private Object[] evaluaCambiarDatoNoObjeto(Class clazz, E auxJ, E auxJ1, TipoOrdenacion tipoOrdenacion, int j) throws Exception {
        Object aux[] = new Object[2];//aux[0];--->null
        if (clazz.getSuperclass().getSimpleName().equalsIgnoreCase("Number")) {
            // Number datoJ = (Number) auxJ;
            // Number datoJ1 = (Number) auxJ1;
            if (tipoOrdenacion.toString().equalsIgnoreCase(TipoOrdenacion.ASCENDENTE.toString())) {
                if ((((Number) auxJ).doubleValue() > ((Number) auxJ1).doubleValue())) {
                    aux[0] = auxJ1;
                    aux[1] = j;
                    //  cambioBurbuja(j, matriz);
                }
            } else {
                if ((((Number) auxJ).doubleValue() < ((Number) auxJ1).doubleValue())) {
                    // cambioBurbuja(j, matriz);
                    aux[0] = auxJ1;
                    aux[1] = j;
                }
            }
        } else if (Utilidades.isString(clazz)) {
            String datoJ = (String) auxJ;
            String datoJ1 = (String) auxJ1;
            if (tipoOrdenacion.toString().equalsIgnoreCase(TipoOrdenacion.ASCENDENTE.toString())) {
                if ((datoJ.toLowerCase().compareTo(datoJ1.toLowerCase()) > 0)) {
                    //cambioBurbuja(j, matriz);
                    aux[0] = auxJ1;
                    aux[1] = j;
                }
            } else {
                if ((datoJ.toLowerCase().compareTo(datoJ1.toLowerCase()) < 0)) {
                    //cambioBurbuja(j, matriz);
                    aux[0] = auxJ1;
                    aux[1] = j;
                }
            }

        } else if (Utilidades.isCharacter(clazz)) {
            Character datoJ = (Character) auxJ;
            Character datoJ1 = (Character) auxJ1;
            if (tipoOrdenacion.toString().equalsIgnoreCase(TipoOrdenacion.ASCENDENTE.toString())) {
                if (datoJ > datoJ1) {
                    //cambioBurbuja(j, matriz);
                    aux[0] = auxJ1;
                    aux[1] = j;
                }
            } else {
                if (datoJ < datoJ1) {
                    //cambioBurbuja(j, matriz);
                    aux[0] = auxJ1;
                    aux[1] = j;
                }
            }

        }
        return aux;
    }

    /**
     * Permite hacer el cambio con datos que son objetos del modelo
     *
     * @param clazz El tipo de clase del atributo
     * @param auxJ dato en la posicion J
     * @param auxJ1 dato en la posicion J + 1
     * @param method El metodo get de J
     * @param method1 El metodo get de J + 1
     * @param tipoOrdenacion El tipo de ordenacion si es Ascendente o
     * Descendente
     * @param j posicion
     * @throws Exception
     */
    private Object[] evaluaCambiarDato(Class clazz, E auxJ, E auxJ1, Method method, Method method1, TipoOrdenacion tipoOrdenacion, int j) throws Exception {
        Object aux[] = new Object[2];
        if (clazz.getSuperclass().getSimpleName().equalsIgnoreCase("Number")) {
            Number datoJ = (Number) method.invoke(auxJ);
            Number datoJ1 = (Number) method1.invoke(auxJ1);
            if (tipoOrdenacion.toString().equalsIgnoreCase(TipoOrdenacion.ASCENDENTE.toString())) {
                if ((datoJ.doubleValue() > datoJ1.doubleValue())) {
                    // cambioBurbuja(j, matriz);
                    aux[0] = auxJ1;
                    aux[1] = j;
                }
            } else {
                if ((datoJ.doubleValue() < datoJ1.doubleValue())) {
                    //    cambioBurbuja(j, matriz);
                    aux[0] = auxJ1;
                    aux[1] = j;
                }
            }
        } else if (Utilidades.isString(clazz)) {
            String datoJ = (String) method.invoke(auxJ);
            String datoJ1 = (String) method1.invoke(auxJ1);
            if (tipoOrdenacion.toString().equalsIgnoreCase(TipoOrdenacion.ASCENDENTE.toString())) {
                if ((datoJ.toLowerCase().compareTo(datoJ1.toLowerCase()) > 0)) {
                    //   cambioBurbuja(j, matriz);
                    aux[0] = auxJ1;
                    aux[1] = j;
                }
            } else {
                if ((datoJ.toLowerCase().compareTo(datoJ1.toLowerCase()) < 0)) {
                    //  cambioBurbuja(j, matriz);
                    aux[0] = auxJ1;
                    aux[1] = j;
                }
            }

        } else if (Utilidades.isCharacter(clazz)) {
            Character datoJ = (Character) method.invoke(auxJ);
            Character datoJ1 = (Character) method1.invoke(auxJ1);
            if (tipoOrdenacion.toString().equalsIgnoreCase(TipoOrdenacion.ASCENDENTE.toString())) {
                if (datoJ > datoJ1) {
                    // cambioBurbuja(j, matriz);
                    aux[0] = auxJ1;
                    aux[1] = j;
                }
            } else {
                if (datoJ < datoJ1) {
                    //  cambioBurbuja(j, matriz);
                    aux[0] = auxJ1;
                    aux[1] = j;
                }
            }

        }
        return aux;
    }

    /**
     * Metodo para busqueda secuencial
     *
     * @param atributo el atributo donde deseo buscar
     * @param dato El dato a buscar
     * @return ListaEnlazada<E> El listado de datos a buscar
     */
    public ListaEnlazada<E> buscar(String atributo, Object dato) throws Exception {
        Class<E> clazz = null;
        E[] matriz = null;
        ListaEnlazada<E> resultado = new ListaEnlazada<>();
        if (size > 0) {
            matriz = toArray();

            clazz = (Class<E>) cabecera.getDato().getClass();//primitivo, Dato envolvente, Object
            Boolean isObject = Utilidades.isObject(clazz);//si es objeto
            if (isObject) {
                Field field = Utilidades.getField(atributo, clazz);
//                Method method = getMethod("get" + Utilidades.capitalizar(atributo), field.getClass());

                for (int i = 0; i < matriz.length; i++) {
                    Method method1 = getMethod("get" + Utilidades.capitalizar(atributo), matriz[i].getClass());
                    E aux = buscarDatoPosicionObjeto(i, matriz, field.getType(), dato, method1);
                    if (aux != null) {
                        resultado.insertar(aux);
                    }
                }
            } else {
                for (int i = 0; i < matriz.length; i++) {
                    E aux = buscarDatoPosicion(i, matriz, clazz, (E) dato);
                    if (aux != null) {
                        resultado.insertar(aux);
                    }
                }
            }

        }
        return resultado;
    }

    //5  7  9 9  12   15   18   buscar 9
    //
    private E buscarDatoPosicion(Integer j, E[] matriz, Class<E> clazz, E dato) throws Exception {
        E aux = null;
        if (clazz.getSuperclass().getSimpleName().equalsIgnoreCase("Number")) {
            Number datoJ = (Number) dato;
            Number datoJ1 = (Number) matriz[j];
            if (datoJ.doubleValue() == datoJ1.doubleValue()) {
                aux = (E) datoJ1;
            }
        } else if (Utilidades.isString(clazz)) {
            String datoJ = (String) dato;
            String datoJ1 = (String) matriz[j];

            if (datoJ1.toLowerCase().startsWith(datoJ.toLowerCase())
                    || datoJ1.toLowerCase().endsWith(datoJ.toLowerCase())
                    || datoJ1.toLowerCase().equalsIgnoreCase(datoJ.toLowerCase())) {
                //cambioBurbuja(j, matriz);
                aux = (E) matriz[j];
            }

        } else if (Utilidades.isCharacter(clazz)) {
            Character datoJ = (Character) dato;
            Character datoJ1 = (Character) matriz[j];
            if (datoJ.charValue() == datoJ1.charValue()) {
                aux = (E) matriz[j];
            }

        }
        return aux;
    }

    private E buscarDatoPosicionObjeto(Integer j, E[] matriz, Class clazz, Object dato, Method method1) throws Exception {
        E aux = null;
        if (clazz.getSuperclass().getSimpleName().equalsIgnoreCase("Number")
                && dato.getClass().getSuperclass().getSimpleName().equalsIgnoreCase("Number")) {
            Number datoJ = (Number) dato;
            Number datoJ1 = (Number) method1.invoke(matriz[j]);
            if (datoJ.doubleValue() == datoJ1.doubleValue()) {
                aux = (E) matriz[j];
            }
        } else if (Utilidades.isString(clazz) && Utilidades.isString(dato.getClass())) {
            String datoJ = (String) dato;
            String datoJ1 = (String) method1.invoke(matriz[j]);

            if (datoJ1.toLowerCase().startsWith(datoJ.toLowerCase())
                    || datoJ1.toLowerCase().endsWith(datoJ.toLowerCase())
                    || datoJ1.toLowerCase().equalsIgnoreCase(datoJ.toLowerCase())) {
                //cambioBurbuja(j, matriz);
                aux = (E) matriz[j];
            }

        } else if (Utilidades.isCharacter(clazz) && Utilidades.isCharacter(dato.getClass())) {
            Character datoJ = (Character) dato;
            Character datoJ1 = (Character) method1.invoke(matriz[j]);
            if (datoJ.charValue() == datoJ1.charValue()) {
                aux = (E) matriz[j];
            }

        }
        return aux;
    }

//    public static void main(String[] args) {
//
//        try {
//
////            ListaEnlazada<String> lista = new ListaEnlazada<>();
////            FileReader fr = new FileReader("datos" + File.separatorChar + "numero.txt");
////            BufferedReader entrada = new BufferedReader(fr);
////            String auxiliar = entrada.readLine();
////            Integer cont = 0;
////            while (auxiliar != null) {
////                //matriz[cont] = Integer.parseInt(auxiliar);
////                //lista.insertar(auxiliar);
////                auxiliar = entrada.readLine();
////                cont++;
////            }
////            fr.close();
////            entrada.close();
//            ListaEnlazada<Factura> lista = new FacturaServicio().getListaArchivo().getLista();
//            lista.imprimir();
//            System.out.println("SELECCION");
//            lista.ordenar_seleccion("total", TipoOrdenacion.DESCENDENTE);
//            lista.imprimir();
//            ListaEnlazada<Factura> busAux = lista.buscar("total", 78.0);
//            System.out.println("--------------------------------------------------");
//            busAux.imprimir();
//            System.out.println("--------------------------------------------------");
////            ListaEnlazada<Double> lista1 = new ListaEnlazada<>();
////            lista1.insertar(9.3);
////            lista1.insertar(4.1);
////            lista1.insertar(1.0);
////            lista1.insertar(9.3);
////            lista1.insertar(4.0);
////            lista1.insertar(4.1);
////            lista1.ordenar_seleccion("", TipoOrdenacion.ASCENDENTE);
////            lista1.imprimir();
////            ListaEnlazada<Double> busqueda = lista1.buscar("", 4.0);
////            busqueda.imprimir();
//        } catch (Exception e) {
//            System.out.println("erro " + e);
//        }
//
//    }
}
