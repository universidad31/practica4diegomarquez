/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.persona;
 

/**
 *
 * @author sebastian
 */
public class Utiles { 
    public static Double calcularSubtotal(Double valor) {
        return redondear(valor / 1.12);
    }
    public static Double calcularIVA(Double valor) {
        return redondear(valor - (calcularSubtotal(valor)));
    }
    public static Double redondear(Double numero) {
        return Math.round(numero * 100.0)/100.0;
    }

     

     
    
    public static Boolean comprobarCadena(String txt) {
        return txt.trim().isEmpty();
    }
    
}
