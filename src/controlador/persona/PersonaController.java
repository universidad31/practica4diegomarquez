/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.persona;

import controlador.DAO.AdaptadorDao;
import controlador.tda.lista.ListaEnlazada;
import controlador.tda.lista.ListaEnlazadaServices;
import controlador.utiles.Utilidades;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Iterator;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import modelo.persona.Persona; 
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author sebastian
 */
public class PersonaController extends AdaptadorDao<Persona>{

    private Persona autor;
    private ListaEnlazadaServices<Persona> listaAutores;

    public PersonaController() {
        super(Persona.class);
        listado();
    }
    
   

    public ListaEnlazadaServices<Persona> getListaAutores() {
        
        return listaAutores;
    }

    public void setListaAutores(ListaEnlazadaServices<Persona> listaAutores) {
        this.listaAutores = listaAutores;
    }

    public Persona getAutor() {
        if (this.autor == null) {
            this.autor = new Persona();
        }
        return autor;
    }

    public void setAutor(Persona autor) {
        this.autor = autor;
    }

    public Boolean guardar() {
        try {            
            getAutor().setId(listaAutores.getSize()+1);
            guardar(getAutor());
            return true;
        } catch (Exception e) {
            System.out.println("Error en guardar autor"+e);
        }
        return false;
    }
    
    public Boolean modificar(Integer pos) {
        try {            
            
            modificar(getAutor(), pos);
            return true;
        } catch (Exception e) {
            System.out.println("Error en modificar autor"+e);
        }
        return false;
    }
    
    public ListaEnlazadaServices<Persona> listado() {
        setListaAutores(listar());
        return listaAutores;
    }

    
}
