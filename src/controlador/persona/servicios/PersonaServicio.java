/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.persona.servicios;

import controlador.tda.lista.ListaEnlazadaServices;
import controlador.persona.PersonaController;
import modelo.persona.Persona;

/**
 *
 * @author sebastian
 */
public class PersonaServicio {
    private PersonaController ac = new PersonaController();
    public Persona getAutor() {
        return ac.getAutor();
    }
    
    public ListaEnlazadaServices<Persona> getLista() {
        return ac.getListaAutores();
    }
    
    public ListaEnlazadaServices<Persona> getListaArchivo() {
        return ac.listado();
    }
    
    public Boolean guardar() {
       return ac.guardar();
    }
    
    public Boolean modificar(Integer pos) {
        return ac.modificar(pos);
    }
    
    public void setAutor(Persona autor) {
        ac.setAutor(autor);
    }
    
}
